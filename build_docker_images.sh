#!/bin/bash
set -e

./build_all.sh

cd shopfront
VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
docker build --force-rm=true --build-arg VERSION=$VERSION -t danielbryantuk/djshopfront:$VERSION .
cd ..

cd productcatalogue
VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
docker build --force-rm=true --build-arg VERSION=$VERSION -t danielbryantuk/djproductcatalogue:$VERSION .
cd ..

cd stockmanager
VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
docker build --force-rm=true --build-arg VERSION=$VERSION -t danielbryantuk/djstockmanager:$VERSION .
cd ..
