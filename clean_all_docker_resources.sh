#!/bin/bash

set -e

#stop and rm all app containers
containers=$(docker ps -a | grep danielbryantuk/ | awk '{print $1}')
if [ ! -z "$containers" ] ; then
 echo $containers | xargs docker stop
 echo $containers | xargs docker rm
fi

#rm all app images
images=$(docker images -a | grep danielbryantuk/ | awk '{print $3}')
if [ ! -z "$images" ] ; then
 echo $images | xargs docker rmi -f
fi

#rm all app network
networks=$(docker network ls | grep danielbryantuk/ | awk '{print $1}')
if [ ! -z "$networks" ] ; then
 echo $networks | xargs docker network rm
fi
