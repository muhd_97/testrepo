#!/bin/bash
set -e

usage () {
  echo "Usage: ./deploy_app_to_eks.sh -v [version] -e [ecr url] -r [region] -n [namespace] -c [cluster name]";
  echo "Example: ./deploy_app_to_eks.sh -v 1.0.0 -e 123456789012.dkr.ecr.ap-southeast-1.amazonaws.com -r ap-southeast-1 -n qat -c mycluster";
}

while getopts v:e:r:n:c:h option
do
  case "$option" in
    v) VERSION=$OPTARG;;
    e) ECR=$OPTARG;;
    r) REGION=$OPTARG;;
    n) NAMESPACE=$OPTARG;;
    c) CLUSTER=$OPTARG;;
    h) usage; exit;;
    \?) usage; exit 1;;
    :) usage; exit 1;;
  esac
done

if [ -z "$VERSION" ] || [ -z "$ECR" ] || [ -z "$REGION" ] || [ -z "$NAMESPACE" ] || [ -z "$CLUSTER" ]; then
    echo "ERROR: Missing option"
    usage
    exit 1
fi

set -e

#note: there is no VPC endpoint service for eks as of April 2020 i.e. the following call will fail if there is no access to internet.
aws eks --region ap-southeast-1 update-kubeconfig --name $CLUSTER

#note: if there is no access to internet, it is still possible to invoke kubectl with a bearer token, cluster API server endpoint and cluster certificate authority certificate
#BEARER_TOKEN=$(aws --region ${bamboo.aws.qat.region} eks get-token --cluster-name ${bamboo.aws.qat.eks.cluster.name} | jq .status.token)
#echo "$ENCODEDCA" | base64 --decode > ca.crt

cat app-deployment.yaml | sed -e "s/#namespace#/$NAMESPACE/g" | sed -e "s/#version#/$VERSION/g" | sed -e "s/#ecrurl#/$ECR/g" > app-deployment-mod.yaml
sed -e "s/#namespace#/$NAMESPACE/g" app-service.yaml > app-service-mod.yaml

#cat app-deployment.yaml
#cat app-service.yaml

kubectl apply -f app-deployment-mod.yaml
kubectl apply -f app-service-mod.yaml

#kubectl --v=0 --kubeconfig=/dev/null --server=${bamboo.aws.qat.eks.cluster.api.server.endpoint} --token=$BEARER_TOKEN --certificate-authority=ca.crt apply -f app-deployment-mod.yml
#kubectl --v=0 --kubeconfig=/dev/null --server=${bamboo.aws.qat.eks.cluster.api.server.endpoint} --token=$BEARER_TOKEN --certificate-authority=ca.crt apply -f app-service-mod.yml

SHOPFRONTURL=$(kubectl get svc --namespace qat | grep shopfront | awk '{print $4}')
echo "Shopfront URL: $SHOPFRONTURL"

echo "Fetching URL of shopfront service"
retries=0
while [ -z "$(kubectl get svc --namespace qat | grep shopfront | awk '{print $4}' | grep amazonaws)" ]
do
  sleep 5
  echo -n "."
  if [ $retries -eq 20 ]
  then
    echo "Timeout waiting for shopfront service to be ready - aborting."
    exit 1
  fi
  retries=$(($retries+1))
done

echo "Waiting for shopfront to be ready"
retries=0
while [ -z "$(curl http://$SHOPFRONTURL:8010/info | grep $VERSION)" ]
do
  sleep 5
  echo -n "."
  if [ $retries -eq 20 ]
  then
    echo "Timeout waiting for shopfront to be ready - aborting."
    exit 1
  fi
  retries=$(($retries+1))
done

echo "shopfront is ready"
