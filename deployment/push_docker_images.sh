#!/bin/bash

usage () {
  echo "Usage: ./push_docker_images.sh -v [version] -e [ecr url] -r [region]";
  echo "Example: ./push_docker_images.sh -v 1.0.0 -e 123456789012.dkr.ecr.ap-southeast-1.amazonaws.com -r ap-southeast-1";
}

logout() {
  docker logout $1
}

while getopts v:e:r:h option
do
  case "$option" in
    v) VERSION=$OPTARG;;
    e) ECR=$OPTARG;;
    r) REGION=$OPTARG;;
    h) usage; exit;;
    \?) usage; exit 1;;
    :) usage; exit 1;;
  esac
done

if [ -z "$VERSION" ] || [ -z "$ECR" ] || [ -z "$REGION" ]; then
    echo "ERROR: Missing option"
    usage
    exit 1
fi

eval "$(aws ecr get-login --region $REGION | sed -e 's/-e none//g')"

docker tag danielbryantuk/djshopfront:$VERSION $ECR/danielbryantuk/djshopfront:$VERSION
if [ $? -ne 0 ]; then
  echo "docker tag fails for danielbryantuk/djshopfront:$VERSION"
  logout $ECR
  exit 1
fi

docker push $ECR/danielbryantuk/djshopfront:$VERSION
if [ $? -ne 0 ]; then
  echo "docker push fails for danielbryantuk/djshopfront:$VERSION"
  logout $ECR
  exit 1
fi

docker tag danielbryantuk/djproductcatalogue:$VERSION $ECR/danielbryantuk/djproductcatalogue:$VERSION
if [ $? -ne 0 ]; then
  echo "docker tag fails for danielbryantuk/djproductcatalogue:$VERSION"
  logout $ECR
  exit 1
fi

docker push $ECR/danielbryantuk/djproductcatalogue:$VERSION
if [ $? -ne 0 ]; then
  echo "docker push fails for danielbryantuk/djproductcatalogue:$VERSION"
  logout $ECR
  exit 1
fi

docker tag danielbryantuk/djstockmanager:$VERSION $ECR/danielbryantuk/djstockmanager:$VERSION
if [ $? -ne 0 ]; then
  echo "docker tag fails for danielbryantuk/djstockmanager:$VERSION"
  logout $ECR
  exit 1
fi

docker push $ECR/danielbryantuk/djstockmanager:$VERSION
if [ $? -ne 0 ]; then
  echo "docker push fails for danielbryantuk/djstockmanager:$VERSION"
  logout $ECR
  exit 1
fi

logout $ECR
