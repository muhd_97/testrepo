#!/bin/bash
set -e

usage () {
  echo "Usage: ./save_docker_images.sh [tag]]";
  echo "Example: ./save_docker_images.sh 1.0.0";
}

if [ $# -ne 1 ] ; then
    echo "ERROR: 1 argument expected"
    usage
    exit 1
fi

TAG=$1

docker save -o docker-image-danielbryantuk-djshopfront-$TAG.tar danielbryantuk/djshopfront:$TAG
docker save -o docker-image-danielbryantuk-djproductcatalogue-$TAG.tar danielbryantuk/djproductcatalogue:$TAG
docker save -o docker-image-danielbryantuk-djstockmanager-$TAG.tar danielbryantuk/djstockmanager:$TAG
