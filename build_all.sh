#!/bin/bash

cd shopfront
mvn clean install surefire-report:report
cd ..
cd productcatalogue
mvn clean install surefire-report:report
cd ..
cd stockmanager
mvn clean install surefire-report:report 
cd ..
