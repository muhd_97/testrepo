#!/bin/bash
set -e

usage () {
  echo "Usage: ./run_docker_app.sh x.y.z";
  echo "Example: ./run_docker_app.sh 1.0.0";
}

if [ $# -ne 1 ] ; then
    echo "ERROR: 1 argument expected"
    usage
    exit 1
fi

VERSION=$1

docker network create danielbryantuk/network
docker run -p 8020:8020 --network danielbryantuk/network -d --name productcatalogue danielbryantuk/djproductcatalogue:$VERSION
docker run -p 8030:8030 --network danielbryantuk/network -d --name stockmanager danielbryantuk/djstockmanager:$VERSION
docker run -p 8010:8010 --network danielbryantuk/network -d --name shopfront danielbryantuk/djshopfront:$VERSION

echo "Waiting for shopfront to be ready"
retries=0
while [ -z "$(curl http://localhost:8010/health | grep 'UP')" ]
do
  sleep 5
  echo -n "."
  if [ $retries -eq 20 ]
  then
    echo "Timeout waiting for shopfront to be ready - aborting."
    exit 1
  fi
  retries=$(($retries+1))
done

echo "shopfront is ready"
