#!/bin/bash
set -e

usage () {
  echo "Usage: ./version_all.sh x.y.z";
  echo "Example: ./version_all.sh 1.0.0";
}

if [ $# -ne 1 ] ; then
    echo "ERROR: 1 argument expected"
    usage
    exit 1
fi

VERSION=$1

cd shopfront
mvn versions:set -DnewVersion=$VERSION -B
cd ..
cd productcatalogue
mvn versions:set -DnewVersion=$VERSION -B
cd ..
cd stockmanager
mvn versions:set -DnewVersion=$VERSION -B
cd ..
