package com.selenium.framework.driver;

import com.google.common.collect.ImmutableMap;
import io.github.bonigarcia.wdm.*;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;

import static com.support.framework.support.Property.*;
import static org.junit.Assert.fail;


@Component
class   SeleniumDriverUtils {

    private static final Logger LOG = Logger.getLogger(SeleniumDriverUtils.class);
    private static ChromeDriverService service;

    @Bean(destroyMethod = "quit")
    @Scope("cucumber-glue")
    @Profile("Selenium")
    private WebDriver getWebDriver()  {
        WebDriver driver;
        TestCapabilities testCapabilities = new TestCapabilities();
        LOG.info("Initializing WebDriver");
        if (GRID_USE.toString().equalsIgnoreCase("true")) {
            driver = new RemoteWebDriver(testCapabilities.getRemoteUrl(), testCapabilities.getDesiredCapabilities());
        } else {
            switch (BROWSER_NAME.toString().toLowerCase()) {
                case "chrome":
                    DriverManagerType chrome = DriverManagerType.CHROME;
                    WebDriverManager.getInstance(chrome).setup();
                    driver = new ChromeDriver(new ChromeOptions());
                    break;
                case "headlesschrome":
                    DriverManagerType headlesschrome = DriverManagerType.CHROME;
                    WebDriverManager.getInstance(headlesschrome).setup();
                    ChromeOptions options = new ChromeOptions();

                    if(getOS().equalsIgnoreCase("Linux")){
                        System.setProperty("webdriver.chrome.driver", LINUX_CHROME_SERVER_DRIVER_PATH.toString());
                        options.addArguments("--headless");
                        options.addArguments("--no-sandbox"); // Bypass OS security model
                        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
                        options.addArguments("nogui=True");
                        options.addArguments("disable-infobars"); // disabling infobars
                        options.addArguments("--disable-extensions"); // disabling extension
                    }else{
                        options.addArguments("headless");
                    }

                    driver = new ChromeDriver(options);
                    break;
                case "firefox":
                    DriverManagerType firefox = DriverManagerType.FIREFOX;
                    WebDriverManager.getInstance(firefox).setup();
                    driver = new FirefoxDriver(new FirefoxOptions());
                    break;
                case "microsoftedge":
                    DriverManagerType microsoftedge = DriverManagerType.EDGE;
                    WebDriverManager.getInstance(microsoftedge).setup();
                    driver = new EdgeDriver(new EdgeOptions());
                    break;
                case "ie":
                    DriverManagerType ie = DriverManagerType.IEXPLORER;
                    WebDriverManager.getInstance(ie).setup();
                    driver = new InternetExplorerDriver(new InternetExplorerOptions());
                    break;
                case "safari":
                    driver = new SafariDriver(new SafariOptions());
                    break;
                case "phantomjs":

                    DesiredCapabilities caps = new DesiredCapabilities();
                    caps.setCapability(CapabilityType.BROWSER_NAME, BrowserType.PHANTOMJS);
                    caps.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
                    caps.setJavascriptEnabled(true);
                    caps.setCapability("locationContextEnabled", true);
                    caps.setCapability("applicationCacheEnabled", true);
                    caps.setCapability("browserConnectionEnabled", true);
                    caps.setCapability("localToRemoteUrlAccessEnabled", true);
                    caps.setCapability("locationContextEnabled", true);
                    caps.setCapability("takesScreenshot", true);


                    if(getOS().equalsIgnoreCase("Linux")){
                        System.setProperty("phantomjs.binary.path", LINUX_PHANTOMJS_SERVER_DRIVER_PATH.toString());
                        //SERVER
                        caps.setCapability(
                                PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                                LINUX_PHANTOMJS_SERVER_DRIVER_PATH.toString()
                        );
                    }
                    driver = new PhantomJSDriver(caps);
                    break;
                default:
                    fail(BROWSER_NAME + " is not found in browser list");
                    driver = null;
                    break;
            }
        }
        driver.manage().deleteAllCookies();
        LOG.info("Resizing browser to: " + BROWSER_WIDTH + "x" + BROWSER_HEIGHT);
        driver.manage().window().setSize(new Dimension(BROWSER_WIDTH.toInt(), BROWSER_HEIGHT.toInt()));
        return driver;
    }

    private String getOS(){
        return System.getProperty("os.name");
    }
}