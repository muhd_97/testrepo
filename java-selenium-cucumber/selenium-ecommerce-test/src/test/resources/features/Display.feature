Feature: Display

  Background: User navigates to Shopfront Page
    Given I am on web page "http://localhost:8010/"

  @sanity
  Scenario: User sees the page header
    Then User sees page header "Welcome to the Docker Java Shopfront!"

  @regression
  Scenario Outline: User sees a table of products with corresponding values
    Then User sees table information "<prodNum>", "<sku>", "<name>", "<description>", "<priceInPounds>", "<qtyAvail>"

    Examples:
      | prodNum | sku      | name      | description          | priceInPounds | qtyAvail |
      | 1       | 12345678 | Widget    | Premium ACME Widgets | 1.20        | 5        |
      | 2       | 34567890 | Sprocket  | Grade B sprockets    | 4.10        | 2        |
      | 3       | 54326745 | Anvil     | Large Anvils         | 45.50       | 999      |
      | 4       | 93847614 | Cogs      | Grade Y cogs         | 1.80        | 0        |
      | 5       | 11856388 | Multitool | Multitools           | 154.10      | 1        |

