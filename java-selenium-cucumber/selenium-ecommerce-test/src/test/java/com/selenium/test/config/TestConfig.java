package com.selenium.test.config;

import org.junit.Assert;

import static com.support.framework.support.Util.stringIsEmpty;

public enum TestConfig {

    WEBPAGE_TITLE("Welcome to the Docker Java Shopfront!"),
    DASHBOARD_WEBPAGE_TITLE("Dashboard / nopCommerce administration"),
    LOGIN_WELCOME_MESSAGE("Welcome, please sign in!");

    private String value;

    TestConfig(String value) {
        this.value = value;
    }

    public boolean toBoolean() {
        if (stringIsEmpty(value)) {
            Assert.fail("TestConfig " + this.name() + " is missing. Please check TestConfig");
        }
        return Boolean.parseBoolean(value);
    }

    public int toInt() {
        if (stringIsEmpty(value)) {
            Assert.fail("TestConfig " + this.name() + " is missing. Please check TestConfig");
        }
        return Integer.parseInt(value);
    }

    public String toString() {
        if (stringIsEmpty(value)) {
            Assert.fail("TestConfig " + this.name() + " is missing. Please check TestConfig");
        }
        return value;
    }
}



