package com.selenium.test.pages;


import com.selenium.framework.base.SeleniumBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.FindBy;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("cucumber-glue")
public class ShopfrontPage extends SeleniumBase {

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[2]/h1[1]")
    public static WebElement PAGE_HEADER;

    public ShopfrontPage(WebDriver driver) {
        super(driver);
        System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
    }

    public int getNumCols(int currentRow) {
        List<WebElement> rowList = this.getDriver().findElements(By.xpath("//*[@id=\"product-table\"]/tbody/tr[" + currentRow + "]/td"));
        return rowList.size();
    }

    public List<String> getProdNumInfo(int prodNum) {
        List<String> result = new ArrayList<String>();
        int numCols = getNumCols(prodNum);

        for (int i = 1; i <= numCols; i++) {
            result.add(this.getDriver().findElement(By.xpath("//*[@id=\"product-table\"]/tbody/tr[" + prodNum + "]/td[" + i + "]")).getText());
        }
        return result;
    }

    public List<String> getTableHeaders() {
        List<String> result = new ArrayList<>();
        int numCols = getNumCols(1);
        for (int i = 1 ; i <= numCols; i++) {
            result.add(this.getDriver().findElement(By.xpath("//*[@id=\"product-table\"]/thead/tr/th[" + i + "]")).getText());
        }
        return result;
    }

    public Map<String, String> getMapOfHeaderToProdNumInfo(int prodNum) {
        Map<String, String> result = new HashMap<>();

        List<String> tableHeaders = getTableHeaders();
        List<String> prodNumInfo = getProdNumInfo(prodNum);
        int colNum = getNumCols(prodNum);

        for (int i = 0; i < colNum; i++) {
            result.put(tableHeaders.get(i), prodNumInfo.get(i));
        }
        return result;
    }


}
