package com.selenium.test.stepdefs;

import cucumber.api.java.en.And;
import org.apache.log4j.Logger;
import com.selenium.test.pages.ShopfrontPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static com.selenium.test.pages.ShopfrontPage.*;
import static com.selenium.test.config.TestConfig.*;



public class ShopfrontPageStepDefs {

    private static final Logger log = Logger.getLogger(ShopfrontPageStepDefs.class);

    @Autowired
    public ShopfrontPage shopfrontPage;

    @Given("^I am on web page \"([^\"]*)\"$")
    public void i_am_on_web_page(String arg1) throws Exception {
        shopfrontPage.getURL("");
        Assert.assertEquals(WEBPAGE_TITLE.toString(), shopfrontPage.getWebPageTitle());
    }

    @Then("^User sees page header \"([^\"]*)\"$")
    public void user_sees_page_header(String arg1) throws Exception {
        shopfrontPage.assertElementPresent(PAGE_HEADER, 10);
    }

    @Then("^User sees table information \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void user_sees_table_information(
            String prodNum, String sku, String name,
            String description, String priceInPounds, String qtyAvail) throws Exception {

        shopfrontPage.waitForPageLoaded(5);
        String prodNumKey = "Product Num";
        String skuKey = "SKU";
        String nameKey = "Name";
        String descriptionKey = "Description";
        String priceKey = "Price £";
        String qtyAvailKey = "Qty Available";

        Map<String, String> prodInfoMap = shopfrontPage.getMapOfHeaderToProdNumInfo(Integer.parseInt(prodNum));
        Assert.assertEquals(prodNum, prodInfoMap.get(prodNumKey));
        Assert.assertEquals(sku, prodInfoMap.get(skuKey));
        Assert.assertEquals(name, prodInfoMap.get(nameKey));
        Assert.assertEquals(description, prodInfoMap.get(descriptionKey));
        Assert.assertEquals(priceInPounds, prodInfoMap.get(priceKey));
        Assert.assertEquals(qtyAvail, prodInfoMap.get(qtyAvailKey));
    }
}
