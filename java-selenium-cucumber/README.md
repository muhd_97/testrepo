# Java-Selenium-Cucumber test script

This test script introduces the java-selenium-cucumber, on how tests are executed, and how logs and reports look like.

Reference from [our java-selenium-cucumber test-automation-starter-kits](https://bitbucket.ship.gov.sg/projects/CLGLAB/repos/test-automation-starter-kits/browse/java-selenium-cucumber)

-------------------

   

#### Run Tests

> Running the tests with the parameters from the text file

    ```mvn clean install -Dbrowser.name=headlesschrome```
    
